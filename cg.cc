#include <legion.h>

#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <random>

using namespace Legion;

const size_t N =
#ifdef N_PP
N_PP;
#else
12;
#endif
const size_t Nb =
#ifdef NB_PP
NB_PP;
#else
4;
#endif

enum TaskIDs {
  TID_TOP_LEVEL,
  TID_INIT_MTX,
  TID_INIT_VEC,
  TID_VEC_PROD,
  TID_VEC_SELF_PROD,
  TID_MTX_VEC_PROD,
  TID_DIVIDE,
  TID_DAXPY,
  TID_VEC_SCALE,
  TID_VEC_PRINT,
#ifdef CG_FINAL_CHECK
  TID_CHECK,
#endif
};

enum FieldIDs {
  FID_MT,
  FID_MA,
  FID_VX,
  FID_VB,
  FID_VR,
  FID_VP,
  FID_VAP
};

void top_level_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, Runtime *runtime)
{
  Rect<2> mtx_rect(Point<2>(0,0), Point<2>(N-1,N-1));
  IndexSpace mtx_is = runtime->create_index_space(ctx, mtx_rect);
  FieldSpace mtx_fs = runtime->create_field_space(ctx);
  {
    FieldAllocator allocator = 
      runtime->create_field_allocator(ctx, mtx_fs);
    allocator.allocate_field(sizeof(double),FID_MA);
  }
  LogicalRegion mtx_lr = runtime->create_logical_region(ctx, mtx_is, mtx_fs);

  IndexSpace vec_is = runtime->create_index_space(ctx, Rect<1>(0,N-1));
  FieldSpace vec_fs = runtime->create_field_space(ctx);
  {
    FieldAllocator allocator =
      runtime->create_field_allocator(ctx, vec_fs);
    allocator.allocate_field(sizeof(double),FID_VX);
    allocator.allocate_field(sizeof(double),FID_VB);
    allocator.allocate_field(sizeof(double),FID_VR);
    allocator.allocate_field(sizeof(double),FID_VP);
    allocator.allocate_field(sizeof(double),FID_VAP);
  }
  LogicalRegion vec_lr = runtime->create_logical_region(ctx, vec_is, vec_fs);

  IndexSpace blk_is = runtime->create_index_space(ctx, Rect<1>(0,Nb-1));
  IndexPartition mtx_ip = runtime->create_equal_partition(ctx, mtx_is, blk_is);
  IndexPartition vec_ip = runtime->create_equal_partition(ctx, vec_is, blk_is);
  LogicalPartition mtx_lp = runtime->get_logical_partition(ctx, mtx_lr, mtx_ip);
  LogicalPartition vec_lp = runtime->get_logical_partition(ctx, vec_lr, vec_ip);

  auto start = std::chrono::steady_clock::now();

  // Initialize A: a random SPD matrix
  TaskLauncher init_launcher(TID_INIT_MTX, TaskArgument(NULL,0));
  init_launcher.add_region_requirement(RegionRequirement(mtx_lr, WRITE_DISCARD, EXCLUSIVE, mtx_lr).add_field(FID_MA));
  runtime->execute_task(ctx, init_launcher);

  // Initialize b: a random vector
  TaskLauncher init_vec_launcher(TID_INIT_VEC, TaskArgument(NULL,0));
  init_vec_launcher.add_region_requirement(RegionRequirement(vec_lr, WRITE_DISCARD, EXCLUSIVE, vec_lr).add_field(FID_VB));
  runtime->execute_task(ctx, init_vec_launcher);

#ifdef DEBUG
  {
    // What's in the matrix?
    InlineLauncher launcher(RegionRequirement(mtx_lr, READ_ONLY, EXCLUSIVE, mtx_lr)
        .add_field(FID_MA));
    PhysicalRegion pr = runtime->map_region(ctx, launcher);
    pr.wait_until_valid();

    const FieldAccessor<READ_ONLY,double,2> acc(pr, FID_MA);
    std::cout << "Matrix: [";
    for (PointInRectIterator<2> pir(mtx_rect); pir(); ++pir)
    {
      std::cout << acc[*pir] << ", ";
    }
    std::cout << "]" << std::endl;
    runtime->unmap_region(ctx, pr);
  }

  {
    TaskLauncher launcher(TID_VEC_PRINT, TaskArgument(NULL,0));
    launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
          EXCLUSIVE, vec_lr).add_field(FID_VB));
    std::cout << "b = ";
    runtime->execute_task(ctx, launcher).get_void_result();
  }
#endif

  // Init x=0
  runtime->fill_field<double>(ctx, vec_lr, vec_lr, FID_VX, 0.0);

  // Init r=b
  {
    CopyLauncher copy;
    copy.add_copy_requirements(
            RegionRequirement(vec_lr, READ_ONLY, EXCLUSIVE, vec_lr)
                .add_field(FID_VB),
            RegionRequirement(vec_lr, WRITE_DISCARD, EXCLUSIVE, vec_lr)
                .add_field(FID_VR));
    runtime->issue_copy_operation(ctx,copy);
  }

  // Init p=r
  {
    CopyLauncher copy;
    copy.add_copy_requirements(
            RegionRequirement(vec_lr, READ_ONLY, EXCLUSIVE, vec_lr)
                .add_field(FID_VR),
            RegionRequirement(vec_lr, WRITE_DISCARD, EXCLUSIVE, vec_lr)
                .add_field(FID_VP));
    runtime->issue_copy_operation(ctx,copy);
  }

  const double tol = std::sqrt(N*std::pow(
        std::numeric_limits<double>::epsilon(), 2));
  const int max_iter = 200;
  const bool neg = true; // will be useful later

  // Compute rTr
  Future rTr;
  {
    TaskLauncher launcher(TID_VEC_SELF_PROD, TaskArgument(NULL,0));
    launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
          EXCLUSIVE, vec_lr).add_field(FID_VR));
    rTr = runtime->execute_task(ctx, launcher);
  }

#ifdef DEBUG
  std::cout << "Initial rTr = " << rTr.get_result<double>() << std::endl;
#endif
  int k = 1;
  while (true)
  {
    // Timing
    std::cout << "k = " << k << ", t = " <<
      std::chrono::duration<double,std::milli>(std::chrono::steady_clock::now()
        - start).count() << std::endl;
    start = std::chrono::steady_clock::now();
    /**
     * Compute Ap
     */
    {
      ArgumentMap arg_map;
      IndexLauncher launcher(TID_MTX_VEC_PROD, blk_is, TaskArgument(NULL,0),
          arg_map);
      launcher.add_region_requirement(RegionRequirement(mtx_lp, 0, READ_ONLY,
            EXCLUSIVE, mtx_lr).add_field(FID_MA));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VP));
      launcher.add_region_requirement(RegionRequirement(vec_lp, 0,
            WRITE_DISCARD, EXCLUSIVE, vec_lr).add_field(FID_VAP));
      runtime->execute_index_space(ctx, launcher);
    }
    /**
     * Compute pTAp
     */
    Future pTap;
    {
      TaskLauncher launcher(TID_VEC_PROD, TaskArgument(NULL,0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VP).add_field(FID_VAP));
      pTap = runtime->execute_task(ctx, launcher);
    }
#ifdef DEBUG
    std::cout << "pTap = " << pTap.get_result<double>() << std::endl;
#endif
    // Compute alpha = rTr / pTap
    Future alpha;
    {
      TaskLauncher launcher(TID_DIVIDE, TaskArgument(NULL,0));
      launcher.add_future(rTr);
      launcher.add_future(pTap);
      alpha = runtime->execute_task(ctx, launcher);
    }
#ifdef DEBUG
    std::cout << "alpha = " << alpha.get_result<double>() << std::endl;

    {
      TaskLauncher launcher(TID_VEC_PRINT, TaskArgument(NULL,0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VP));
      std::cout << "p = ";
      runtime->execute_task(ctx, launcher).get_void_result();
    }
#endif
    // Compute x = x - alpha*p
    {
      TaskLauncher launcher(TID_DAXPY, TaskArgument(NULL, 0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VP));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_WRITE,
            EXCLUSIVE, vec_lr).add_field(FID_VX));
      launcher.add_future(alpha);
      runtime->execute_task(ctx, launcher);
    }
#ifdef DEBUG
    {
      TaskLauncher launcher(TID_VEC_PRINT, TaskArgument(NULL,0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VX));
      std::cout << "x = ";
      runtime->execute_task(ctx, launcher).get_void_result();
    }

    {
      TaskLauncher launcher(TID_VEC_PRINT, TaskArgument(NULL,0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VAP));
      std::cout << "Ap = ";
      runtime->execute_task(ctx, launcher).get_void_result();
    }
#endif
    // Compute r = r - alpha*Ap
    {
      TaskLauncher launcher(TID_DAXPY, TaskArgument(&neg, sizeof(bool)));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VAP));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_WRITE,
            EXCLUSIVE, vec_lr).add_field(FID_VR));
      launcher.add_future(alpha);
      runtime->execute_task(ctx, launcher);
    }
#ifdef DEBUG
    {
      TaskLauncher launcher(TID_VEC_PRINT, TaskArgument(NULL,0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VR));
      std::cout << "rn = ";
      runtime->execute_task(ctx, launcher).get_void_result();
    }
#endif
    // Test new magnitude of r
    Future rTr_new;
    {
      TaskLauncher launcher(TID_VEC_SELF_PROD, TaskArgument(NULL,0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VR));
      rTr_new = runtime->execute_task(ctx, launcher);
    }
    double drTr_new = rTr_new.get_result<double>();
    std::cout << "residual = " << drTr_new << std::endl;
    if (drTr_new < tol) {
      std::cout << "converged after " << k << " iterations!" << std::endl;
      break;
    } else if (k >= max_iter) {
      std::cout << "failed to converge after " << k << " iterations."
          << std::endl;
      break;
    }
    ++k;

    // Prepare for next iteration

    /**
     * Compute beta = rTr_new / rTr
     */
    Future beta;
    {
      TaskLauncher launcher(TID_DIVIDE, TaskArgument(NULL,0));
      launcher.add_future(rTr_new);
      launcher.add_future(rTr);
      beta = runtime->execute_task(ctx, launcher);
    }
#ifdef DEBUG
    double dbeta = beta.get_result<double>();
    std::cout << "beta = " << dbeta << std::endl;
#endif
    /**
     * Compute p = beta*p + r
     */
    // p = beta*p
    {
      TaskLauncher launcher(TID_VEC_SCALE, TaskArgument(NULL, 0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_WRITE,
            EXCLUSIVE, vec_lr).add_field(FID_VP));
      launcher.add_future(beta);
      runtime->execute_task(ctx, launcher);
    }
    // p = p + r
    {
      TaskLauncher launcher(TID_DAXPY, TaskArgument(NULL, 0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VR));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_WRITE,
            EXCLUSIVE, vec_lr).add_field(FID_VP));
      runtime->execute_task(ctx, launcher);
    }
#ifdef DEBUG
    {
      TaskLauncher launcher(TID_VEC_PRINT, TaskArgument(NULL,0));
      launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
            EXCLUSIVE, vec_lr).add_field(FID_VP));
      std::cout << "pkp1 = ";
      runtime->execute_task(ctx, launcher).get_void_result();
    }
#endif
    rTr = rTr_new;
  }

#ifdef CG_FINAL_CHECK
  {
    TaskLauncher launcher(TID_CHECK, TaskArgument(NULL,0));
    launcher.add_region_requirement(RegionRequirement(mtx_lr, READ_ONLY,
          EXCLUSIVE, mtx_lr).add_field(FID_MA));
    launcher.add_region_requirement(RegionRequirement(vec_lr, READ_ONLY,
          EXCLUSIVE, vec_lr).add_field(FID_VX).add_field(FID_VB));
    runtime->execute_task(ctx, launcher);
  }
#endif

  // Cleanup
  runtime->destroy_logical_region(ctx, mtx_lr);
  runtime->destroy_logical_region(ctx, vec_lr);
  runtime->destroy_field_space(ctx, mtx_fs);
  runtime->destroy_field_space(ctx, vec_fs);
  runtime->destroy_index_space(ctx, blk_is);
  runtime->destroy_index_space(ctx, mtx_is);
  runtime->destroy_index_space(ctx, vec_is);
}

void init_vec_task(const Task *task,
                   const std::vector<PhysicalRegion> &regions,
                   Context ctx, Runtime *runtime)
{
  FieldID fid = *(task->regions[0].privilege_fields.begin());

  std::default_random_engine eng(1);
  std::uniform_real_distribution<double> dist(0.0,1.0);

  const FieldAccessor<WRITE_DISCARD,double,1> acc(regions[0], fid);
  // Note here that we get the domain for the subregion for
  // this task from the runtime which makes it safe for running
  // both as a single task and as part of an index space of tasks.
  Rect<1> rect = runtime->get_index_space_domain(ctx,
                  task->regions[0].region.get_index_space());
  for (PointInRectIterator<1> pir(rect); pir(); pir++) {
    acc[*pir] = dist(eng);
  }
}

void init_mtx_task(const Task *task,
                   const std::vector<PhysicalRegion> &regions,
                   Context ctx, Runtime *runtime)
{
  FieldID fid = *(task->regions[0].privilege_fields.begin());

  // Need to store a temp random matrix
  FieldSpace fs = runtime->create_field_space(ctx);
  {
    FieldAllocator allocator = runtime->create_field_allocator(ctx, fs);
    allocator.allocate_field(sizeof(double), FID_MT);
  }
  LogicalRegion lr = runtime->create_logical_region(ctx,
      task->regions[0].region.get_index_space(), fs);
  
  InlineLauncher launcher(RegionRequirement(lr, WRITE_DISCARD, EXCLUSIVE, lr)
      .add_field(FID_MT));
  PhysicalRegion pr = runtime->map_region(ctx, launcher);
  pr.wait_until_valid();

  std::default_random_engine eng(0);
  std::uniform_real_distribution<double> dist(0.0, 1.0);

  const FieldAccessor<WRITE_DISCARD,double,2> acct(pr, FID_MT);
  const FieldAccessor<WRITE_DISCARD,double,2> acca(regions[0], fid);

  Rect<2> rect = runtime->get_index_space_domain(ctx,
                  lr.get_index_space());
  for (PointInRectIterator<2> pir(rect); pir(); pir++) {
    acct[*pir] = dist(eng);
  }

  // Compute (1/2)(A+AT)
  for (size_t r = 0; r < N; r++) {
    for (size_t c = r; c < N; c++) {
      Point<2> pt(r,c), ptt(c,r);
      double d = 0.5*(acct[pt] + acct[ptt]);
      if (r == c) d += std::sqrt(N);
      acca[pt] = d;
      acca[ptt] = d;
    }
  }

  // Cleanup
  runtime->unmap_region(ctx, pr);
  runtime->destroy_logical_region(ctx, lr);
  runtime->destroy_field_space(ctx, fs);
}

double vec_prod_task(const Task *task,
                     const std::vector<PhysicalRegion> &regions,
                     Context ctx, Runtime *runtime)
{
  auto fields_it = task->regions[0].privilege_fields.begin();
  FieldID fida = *(fields_it++),
          fidb = *(fields_it);

  const FieldAccessor<READ_ONLY,double,1> acca(regions[0], fida),
                                          accb(regions[0], fidb);

  double sum = 0.0;
  Rect<1> rect = runtime->get_index_space_domain(ctx,
                 task->regions[0].region.get_index_space());

  for (PointInRectIterator<1> pir(rect); pir(); pir++) {
    sum += acca[*pir] * accb[*pir];
  }

  return sum;
}

double vec_self_prod_task(const Task *task,
                          const std::vector<PhysicalRegion> &regions,
                          Context ctx, Runtime *runtime)
{
  FieldID fid = *(task->regions[0].privilege_fields.begin());

  const FieldAccessor<READ_ONLY,double,1> acc(regions[0], fid);

  double sum = 0.0;
  Rect<1> rect = runtime->get_index_space_domain(ctx,
                 task->regions[0].region.get_index_space());

  for (PointInRectIterator<1> pir(rect); pir(); pir++) {
    double v = acc[*pir];
    sum += (v*v);
  }

  return sum;
}

void mtx_vec_prod_task(const Task *task,
                       const std::vector<PhysicalRegion> &regions,
                       Context ctx, Runtime *runtime)
{
  FieldID fid_m = *(task->regions[0].privilege_fields.begin()),
          fid_vi = *(task->regions[1].privilege_fields.begin()),
          fid_vo = *(task->regions[2].privilege_fields.begin());

  const FieldAccessor<READ_ONLY,double,2> acc_m(regions[0], fid_m);
  const FieldAccessor<READ_ONLY,double,1> acc_vi(regions[1], fid_vi);
  const FieldAccessor<WRITE_DISCARD,double,1> acc_vo(regions[2], fid_vo);

  Rect<2> rect_m = runtime->get_index_space_domain(ctx,
      task->regions[0].region.get_index_space());
  Rect<1> rect_vi = runtime->get_index_space_domain(ctx,
      task->regions[1].region.get_index_space());
  Rect<1> rect_vo = runtime->get_index_space_domain(ctx,
      task->regions[2].region.get_index_space());

  PointInRectIterator<2> pir_m(rect_m, false);
  for (PointInRectIterator<1> pir_vo(rect_vo); pir_vo(); ++pir_vo) {
    acc_vo[*pir_vo] = 0.0;
    for(PointInRectIterator<1> pir_vi(rect_vi); pir_vi(); ++pir_vi) {
      double add = acc_vi[*pir_vi] * acc_m[*pir_m];
      acc_vo[*pir_vo] = acc_vo[*pir_vo] + add;
      pir_m++;
    }
  }
}

double divide_task(const Task *task,
                   const std::vector<PhysicalRegion> &regions,
                   Context ctx, Runtime *runtime)
{
  Future f1 = task->futures[0];
  double r1 = f1.get_result<double>();
  Future f2 = task->futures[1];
  double r2 = f2.get_result<double>();

  double quot = r1 / r2;

#ifdef DEBUG_DIV
  std::cout << "divide, r1 = " << r1 << ", r2 = " << r2 << ", q = " << quot <<
    std::endl;
#endif

  return quot;
}

void daxpy_task(const Task *task,
                const std::vector<PhysicalRegion> &regions,
                Context ctx, Runtime *runtime)
{
  double alpha;
  if (task->futures.size() == 1) {
    alpha = task->futures[0].get_result<double>();
  } else {
    alpha = 1.0;
  }

  bool neg;
  if (task->arglen == sizeof(bool)) {
    neg = *(const bool *)task->args;
  } else {
    neg = false;
  }

#ifdef DEBUG_DAXPY
  std::cout << "in daxpy, alpha = " << alpha << ", neg = " << neg << std::endl;
#endif

  if (neg) alpha = -alpha;

  FieldID fidx = *(task->regions[0].privilege_fields.begin()),
          fidy = *(task->regions[1].privilege_fields.begin());

  const FieldAccessor<READ_ONLY,double,1>  accx(regions[0], fidx);
  const FieldAccessor<READ_WRITE,double,1> accy(regions[1], fidy);

  Rect<1> rect = runtime->get_index_space_domain(ctx,
                 task->regions[0].region.get_index_space());

  for (PointInRectIterator<1> pir(rect); pir(); pir++) {
    accy[*pir] = accy[*pir] + alpha*accx[*pir];
  }
}

void vec_scale_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, Runtime *runtime)
{
  FieldID fid = *(task->regions[0].privilege_fields.begin());
  Future fscale = task->futures[0];
  const FieldAccessor<READ_WRITE,double,1> acc(regions[0], fid);

  Rect<1> rect = runtime->get_index_space_domain(ctx,
                 task->regions[0].region.get_index_space());

  double scale = fscale.get_result<double>();

  for (PointInRectIterator<1> pir(rect); pir(); pir++) {
    acc[*pir] = acc[*pir] * scale;
  }
}

void vec_print_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, Runtime *runtime)
{
  FieldID fid = *(task->regions[0].privilege_fields.begin());
  const FieldAccessor<READ_ONLY,double,1> acc(regions[0], fid);

  Rect<1> rect = runtime->get_index_space_domain(ctx,
                 task->regions[0].region.get_index_space());

  std::cout << "[";
  for (PointInRectIterator<1> pir(rect); pir(); ++pir)
  {
    std::cout << acc[*pir] << ", ";
  }
  std::cout << "]" << std::endl;
}

#ifdef CG_FINAL_CHECK
void check_task(const Task *task,
                const std::vector<PhysicalRegion> &regions,
                Context ctx, Runtime *runtime)
{
  /* Region Requirements:
     1) Matrix A (RO)
     2) Vectors X, B (RO)
  */

  FieldID fida = *(task->regions[0].privilege_fields.begin());
  auto r1it = task->regions[1].privilege_fields.begin();
  FieldID fidx = *r1it++, fidb = *r1it;

  const FieldAccessor<READ_ONLY,double,2> acca(regions[0], fida);
  const FieldAccessor<READ_ONLY,double,1> accx(regions[1], fidx),
                                          accb(regions[1], fidb);
  
  Rect<2> mtx_rect = runtime->get_index_space_domain(ctx,
      task->regions[0].region.get_index_space());

  Rect<1> vec_rect = runtime->get_index_space_domain(ctx,
      task->regions[1].region.get_index_space());

  double resid = 0.0;

  PointInRectIterator<2> a_it(mtx_rect, false);

  for (PointInRectIterator<1> b_it(vec_rect); b_it(); ++b_it) {
    double b_el = 0.0;
    for (PointInRectIterator<1> x_it(vec_rect); x_it(); ++x_it, ++a_it) {
      b_el += (acca[*a_it] * accx[*x_it]);
    }
    double diff = b_el - accb[*b_it];
    resid += diff*diff;
  }

  std::cout << "Final residual: " << resid << std::endl;
}
#endif

int main(int argc, char **argv)
{
  Runtime::set_top_level_task_id(TID_TOP_LEVEL);

  {
    TaskVariantRegistrar registrar(TID_TOP_LEVEL, "top_level");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    Runtime::preregister_task_variant<top_level_task>(registrar, "top_level");
  }

  {
    TaskVariantRegistrar registrar(TID_INIT_MTX, "init_mtx");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    Runtime::preregister_task_variant<init_mtx_task>(registrar, "init_mtx");
  }

  {
    TaskVariantRegistrar registrar(TID_INIT_VEC, "init_vec");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<init_vec_task>(registrar, "init_vec");
  }

  {
    TaskVariantRegistrar registrar(TID_VEC_PROD, "vec_prod");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<double, vec_prod_task>(registrar,
        "vec_prod");
  }
  {
    TaskVariantRegistrar registrar(TID_VEC_SELF_PROD, "vec_self_prod");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<double, vec_self_prod_task>(registrar,
        "vec_self_prod");
  }

  {
    TaskVariantRegistrar registrar(TID_MTX_VEC_PROD, "mtx_vec_prod");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<mtx_vec_prod_task>(registrar,
        "mtx_vec_prod");
  }

  {
    TaskVariantRegistrar registrar(TID_DIVIDE, "divide");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<double, divide_task>(registrar,
        "divide");
  }

  {
    TaskVariantRegistrar registrar(TID_DAXPY, "daxpy");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<daxpy_task>(registrar,
        "daxpy");
  }

  {
    TaskVariantRegistrar registrar(TID_VEC_SCALE, "vec_scale");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<vec_scale_task>(registrar,
        "vec_scale");
  }

  {
    TaskVariantRegistrar registrar(TID_VEC_PRINT, "vec_print");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<vec_print_task>(registrar,
        "vec_print");
  }

#ifdef CG_FINAL_CHECK
  {
    TaskVariantRegistrar registrar(TID_CHECK, "check");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<check_task>(registrar,
        "check");
  }
#endif

  return Runtime::start(argc, argv);
}
